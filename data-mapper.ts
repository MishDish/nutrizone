import { ILineInfo, IBranchInfo, IMethodInfo, ISingleFileInfo, IEnvironment, IBuild, IProductionInfo, UsageFrequency } from './data-model';
import { IToken, CoverageType, BranchType, IBuildSession, IQualityStatus } from './data-model';
import { IManualTestCoverage, IReasonsOfFailure } from './data-model';
import { Server } from './server-contracts';
import { ILatestEnvironment } from './index';

export class DataMapper {
    mapLineItemsCoverage(lineItems: Server.ICodeItem[]): ILineInfo[] {
        if (!lineItems)
            return [];

        const serverLines = lineItems.filter(item => item.type === "line");
        const result: ILineInfo[] = serverLines.map((sl: Server.ILine) => this.mapLineCoverage(sl))

        return result;
    }

    mapLineCoverage(line: Server.ILine): ILineInfo {
        if (!line) {
            throw Error("Unable to map server line information to client");
        }

        const lineNumber = Number(line.name);
        const alerts = line.alerts || {
            line_qh: 0,
            line_notCoveredAnywhere: false,
            method_qh: 0,
            method_notCoveredAnywhere: false,
            methodBranch_qh: 0,
            methodBranch_notCoveredAnywhere: false
        };

        const isQh = line.isQh === undefined ? alerts.line_qh > 0 : line.isQh;
        const isRegression = line.isRegression === undefined ? false : line.isRegression;
        const isNotCoveredAnywhere = line.isNotCoveredAnywhere === undefined ? alerts.line_notCoveredAnywhere : line.isNotCoveredAnywhere;

        return {
            lineNumber: isNaN(lineNumber) ? -1 : lineNumber,
            covered: (line.hits > 0),
            hasErrors: (isQh || isRegression || isNotCoveredAnywhere),
            qualityHoles: alerts.line_qh,
            isQh: isQh,
            notCoveredAnywhere: isNotCoveredAnywhere,
            isRegression: isRegression
        }
    }

    mapBranchItemsCoverage(branchItems: Server.ICodeItem[], formatVersion: Server.FormatVersion): IBranchInfo[] {
        if (!branchItems)
            return [];

        if (formatVersion === "1.0") {
            const serverMethods = branchItems.filter(item => item.type === "method");
            const result: IBranchInfo[] = serverMethods
                .map((sm: Server.IMethodNew) => this.mapBranchCoverages(sm))
                .reduce((a, b) => a.concat(b), []);

            return result;
        } else if (formatVersion === "2.0") {
            const serverBranches = branchItems.filter(item => item.type === "branch");
            const result: IBranchInfo[] = serverBranches
                .map(sb => this.mapBranchCoverage(<Server.IMethodBranch><any>sb));

            return result;
        } else {
            throw Error(`Unsupported format version ${formatVersion}`);
        }
    }

    mapBranchCoverages(method: Server.IMethodNew): IBranchInfo[] {
        if (!method) {
            throw Error("Unable to map server method information to client");
        }
        if (!method.methodBranches)
            return [];

        return method.methodBranches.map(mb => this.mapBranchCoverage(mb));
    }

    mapBranchCoverage(branch: Server.IMethodBranch): IBranchInfo {
        if (!branch) {
            throw Error("Unable to map server branch information to client");
        }

        const lineNumber = branch.position && branch.position[0];

        return {
            branchType: BranchType[branch.branchType] || BranchType.Unknown,
            lineNumber: isNaN(lineNumber) ? -1 : lineNumber,
            covered: (branch.hits > 0),
            hasErrors: (branch.isQh || branch.isRegression || branch.isNotCoveredAnywhere),
            qualityHoles: branch.isQh ? 1 : 0,
            isQh: branch.isQh,
            isRegression: branch.isRegression,
            notCoveredAnywhere: branch.isNotCoveredAnywhere
        }
    }

    mapMethodItemsCoverage(methodItems: Server.ICodeItem[]): IMethodInfo[] {
        if (!methodItems)
            return [];

        let serverMethods = methodItems.filter(item => item.type === "method");
        let result: IMethodInfo[] = serverMethods.map((sm: Server.IMethodNew) => this.mapMethodCoverage(sm))
        return result;
    }

    mapMethodCoverage(method: Server.IMethodNew): IMethodInfo {
        if (!method) {
            throw Error("Unable to map server method information to client");
        }

        let startLine = method.position && method.position[0];
        startLine = startLine || -1;
        const endLine = method.endPosition && method.endPosition[0];
        const alerts = method.alerts || {
            method_qh: 0,
            method_notCoveredAnywhere: false
        };

        let coverage: number;
        if (method.totalMethodBranches) {
            coverage = method.methodBranchesCoverage;
        } else {
            coverage = method.hits ? 100 : 0;
        }

        const isQh = method.isQh === undefined ? alerts.method_qh > 0 : method.isQh;
        const isRegression = method.isRegression;
        const isNotCoveredAnywhere = method.isNotCoveredAnywhere === undefined ? alerts.method_notCoveredAnywhere : method.isNotCoveredAnywhere;
        const productionInfo: IProductionInfo = {
            isCovered: false,
            usageFrequency: null
        };

        if (method.production) {
            productionInfo.isCovered = method.production.isCovered;
            productionInfo.usageFrequency = UsageFrequency[method.production.usageFrequency];
        }

        return {
            name: method.name,
            startLineNumber: startLine,
            endLineNumber: endLine ? endLine : startLine,
            covered: (method.hits > 0),
            coverage: coverage,
            hasErrors: (isQh || isRegression || isNotCoveredAnywhere || productionInfo.isCovered),
            qualityHoles: alerts.method_qh,
            isQh: isQh,
            isRegression: isRegression,
            notCoveredAnywhere: isNotCoveredAnywhere,
            productionInfo: productionInfo
        }
    }

    mapCoverageType(coverageTypes: Server.CoverageType[]): CoverageType {
        if (!coverageTypes)
            return CoverageType.Unknown;

        let result = CoverageType.Unknown;

        for (let index = 0; index < coverageTypes.length; index++) {
            let type = coverageTypes[index];

            if (type === "lines")
                result |= CoverageType.Lines;
            else if (type === "methods-and-branches")
                result |= CoverageType.Branches | CoverageType.Methods;
            else if (type === "methods")
                result |= CoverageType.Methods;
            else if (type === "branches")
                result |= CoverageType.Branches;
        }

        return result;
    }

    mapSingleFileInfo(singleFileInfo: Server.IReportResponse): ISingleFileInfo {
        const items = singleFileInfo && singleFileInfo.data && singleFileInfo.data.items;

        if (!items)
            throw Error("Coverage items not available");

        const formatVersion = singleFileInfo.data.formatVersion || "1.0";
        const coverageType = this.mapCoverageType(singleFileInfo.data.coverageType)
        const lines = this.mapLineItemsCoverage(items);
        const branches = this.mapBranchItemsCoverage(items, formatVersion);
        const methods = this.mapMethodItemsCoverage(items);
        //assume accuracy
        const extra = singleFileInfo.meta && singleFileInfo.meta.extra;
        const isAccurate = extra ? extra.isAccurate : true;

        const result: ISingleFileInfo = {
            coverageType: coverageType,
            lineData: lines,
            branchData: branches,
            methodData: methods,
            isAccurate: !!isAccurate,
            buildName: singleFileInfo.data.buildName,
            testStage: singleFileInfo.data.testStage
        };

        return result;
    }

    mapManualTestsToken(token: Server.IToken): IToken {
        return {
            name: token.name || null,
            server: token.server || null,
            token: token.token || null,
            role: token.role,
            type: token.type || null
        };
    }

    mapManualTestCoverage(data: Server.IManualTestsCoverageReportData): IManualTestCoverage {
        return {
            coverageType: data.coverageType ? this.mapCoverageType(data.coverageType) : CoverageType.Unknown,
            methodCoverage: data.methodCoverage ? data.methodCoverage : 0,
            branchCoverage: data.branchesCoverage ? data.branchesCoverage : 0,
            lineCoverage: data.lineCoverage ? data.lineCoverage : 0
        };
    }

    mapBuilds(build: Server.IBuild): IBuild {
        return {
            id: build.id || null,
            customerId: build.customerId || null,
            buildVersion: build.component ? build.component.name || null : null,
            type: build.component ? build.component.type || null : null,
            appName: build.appName || null,
            branchName: build.branchName || null,
            generated: build.generated, // replace with moment
            isFirstBuild: build.isFirstBuild,
            referenceBuildName: build.referenceBuildName || null,
            referenceBuildBranch: build.referenceBuildBranch || null,
            previousBuild: build.previousBuild || null,
            previousBranch: build.previousBranch || null,
            qualityStatus: build.qualityStatus ? this.mapQualityStatus(build.qualityStatus) : null,
            environments: build.environments ? this.mapEnvironments(build.environments) : []
        }
    }

    mapEnvironments(environments: Server.IEnvironment[]): IEnvironment[] {
        const envData: IEnvironment[] = environments.map((environment) => {
            return {
                id: environment.id || '',
                name: environment.environmentName || ''
            }
        });

        return envData;
    }

    mapLatestEnvironments(environments: Server.ILatestEnvironment[]): ILatestEnvironment[] {
        if (!environments || environments.length === 0) {
            return null;
        }
        const envData: ILatestEnvironment[] = environments.map((environment) => {
            return {
                name: environment.environmentName || null,
                buildName: environment.buildName || null,
                branchName: environment.branchName || null,
                // if isAggregated property doesn't exist, assume it's false
                isAggregated: environment.isAggregated !== undefined ? environment.isAggregated : false
            }
        });

        return envData;
    }

    mapQualityStatus(qs: IQualityStatus): IQualityStatus {
        return {
            status: qs.status || null,
            reasonsOfFailure: this.mapReasonsOfFailure(qs.reasonsOfFailure)
        };
    }

    mapReasonsOfFailure(reasonsOfFailure: Server.IReasonsOfFailure[]): IReasonsOfFailure[] {
        if (!reasonsOfFailure || reasonsOfFailure.length === 0) {
            return [];
        }
        return reasonsOfFailure.map(rf => (
            {
                actualValue: rf.actualValue || null,
                value: rf.value || null,
                operator: rf.operator || null,
                field: rf.field || null
            }
        ));
    }

    mapBuildSession(buildSession: Server.IBuildSession): IBuildSession {
        return {
            customerId: buildSession.customerId || null,
            appName: buildSession.appName || null,
            buildName: buildSession.buildName || null,
            branchName: buildSession.branchName || null,
            buildSessionId: buildSession.buildSessionId || null
        }
    }
}